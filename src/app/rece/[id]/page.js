import Image from "next/image";
import Link from "next/link";

// CSS Ressources
import s from "./styles.module.css";

// Ressources
import iconSave from "../../../../public/icons/icon_save.svg";
import iconShare from "../../../../public/icons/icon_share.svg";

// Composants --------------------
const RatingComp = ({ val }) => {
  return (
    <div className={s.rating_comp}>
      <p>{val}</p>
      <span>
        <svg
          width={14.583}
          height={14}
          viewBox="0 0 15 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
            fill="#FFEC44"
            stroke="#FFEC44"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
        <svg
          width={14.583}
          height={14}
          viewBox="0 0 15 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
            fill="#FFEC44"
            stroke="#FFEC44"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
        <svg
          width={14.583}
          height={14}
          viewBox="0 0 15 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
            fill="#FFEC44"
            stroke="#FFEC44"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
        <svg
          width={14.583}
          height={14}
          viewBox="0 0 15 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
            fill="#FFEC44"
            stroke="#FFEC44"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
        <svg
          width={14.583}
          height={14}
          viewBox="0 0 15 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
            fill="#CCCCCC"
            stroke="#CCCCCC"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </span>
    </div>
  );
};
export default function Page() {
  return (
    <div className={s.recipe_container}>
      <header className={s.recipe_header}>
        <span className={s.cont_author}>
          <Link href="/user/jeissonleon">
            <Image
              src={"/pic-profile.png"}
              width={32}
              height={32}
              alt="Author pic"
            />
          </Link>
          <div>
            <h1 className={s.tit_recipe}>Coq au Vin</h1>
            <p>By Jeisson Leon</p>
          </div>
        </span>

        {/* Image principale */}
        <div className={s.hde_cont_img}>
          <Image
            src={"/Coq-Au-Van.jpg"}
            width={500}
            height={500}
            alt="Recette"
          />
        </div>

        {/* Brève description */}
        <div className={s.cont_tx_info}>
          <p>
            Un classique français de poulet cuit au vin rouge et aux herbes
            aromatiques.
          </p>
          <p>
            A coq au vin is a classic French stew in which chicken is braised
            slowly in red wine and a little brandy to yield a supremely rich
            sauce filled with tender meat, crisp bits of bacon, mushrooms and
            burnished pearl onions.
          </p>
        </div>
        <div className={s.cont_list_info}>
          <h3>Temp de preparation:</h3>
          <p>Eviron 2 heures</p>
        </div>
        <div className={s.cont_list_info}>
          <h3>Portions:</h3>
          <p>4</p>
        </div>
        <div className={s.cont_list_info}>
          <h3>Difficulté:</h3>
          <p>Moyenne</p>
        </div>

        <span className={s.ry_div_hder} />

        <div className={s.hder_cont_btns}>
          <RatingComp val={4.5} />

          <button className={s.btn_save}>
            <Image src={iconSave} alt="Save" />
          </button>
          <button className={s.btn_share}>
            <Image src={iconShare} alt="Share" />
          </button>
        </div>
      </header>

      <span className={s.ry_div} />

      {/* Seccion de comments */}
      <section className={s.recipe_content}>
        <div className={s.cont_ingre}>
          <h2 className={s.ingre_title}>Ingrédients</h2>
          <ul>
            {/* Lista de ingredientes en francés */}
            <li>- Poulet (portions individuelles ou poulet entier).</li>
            <li>- Vin rouge.</li>
            <li>- Champignons.</li>
            <li>- Oignon.</li>
            <li>- Carottes.</li>
            <li>- Herbes aromatiques (thym, laurier, etc.).</li>
            <li>- Huile d&apos;olive.</li>
            <li>- Sel et poivre.</li>
          </ul>
        </div>

        <div className={s.cont_instru}>
          <h2 className={s.instru_title}>Instructions de préparatión</h2>
          <ol>
            {/* Pasos de preparación en francés */}
            <li>
              <h3>Étape 1</h3> Faire dorer le poulet dans de l&apos;huile
              d&apos;olive.
            </li>
            <li>
              <h3>Étape 2</h3> Faire sauter les légumes.
            </li>
            <li>
              <h3>Étape 3</h3> Ajouter le vin rouge et les herbes.
            </li>
            <li>
              <h3>Étape 4</h3> Cuire à feu doux jusqu&apos;à ce que le poulet
              soit tendre et que le vin réduise.
            </li>
          </ol>
        </div>
      </section>

      <section className={s.cont_imgs_desc}>
        {/* Imágenes descriptivas */}
        <div className={s.card_desc}>
          <Image
            src="/des_coqauvin.jpeg"
            width="200"
            height="200"
            alt="Image descriptive"
          />
          Imagen descriptiva de prueba
        </div>
      </section>

      <section className="comentarios">
        {/* Sección de comentarios */}
        {/* Implementa un componente de comentarios o formulario de comentarios en francés */}
      </section>

      <section className="recetas-relacionadas">
        {/* Recetas relacionadas en francés */}
        {/* Muestra recetas relacionadas o sugerencias en francés */}
      </section>
    </div>
  );
}
