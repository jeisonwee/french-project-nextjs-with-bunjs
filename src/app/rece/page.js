"use client";

import Image from "next/image";
import Link from "next/link";

// CSS Ressources
import s from "./styles.module.css";

// Ressources
import iconSave from "../../../public/icons/icon_save.svg";

/* */
const RatingComp = ({ val }) => {
	return (
		<div className={s.rating_comp}>
			<svg
				width={14.583}
				height={14}
				viewBox="0 0 15 14"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
			>
				<path
					d="M6.772 1.648c.164-.37.247-.554.362-.61.1-.05.216-.05.316 0 .114.056.197.24.362.61l1.312 2.944a.773.773 0 0 0 .11.205.355.355 0 0 0 .12.087c.051.023.11.03.23.042l3.205.338c.402.043.603.064.693.156a.35.35 0 0 1 .097.3c-.018.127-.168.262-.47.533l-2.393 2.158a.79.79 0 0 0-.161.169.356.356 0 0 0-.046.14.784.784 0 0 0 .031.232l.669 3.152c.084.396.126.594.066.707a.356.356 0 0 1-.255.186c-.127.021-.302-.08-.652-.282l-2.792-1.61a.785.785 0 0 0-.21-.101.358.358 0 0 0-.148 0 .785.785 0 0 0-.21.101l-2.792 1.61c-.35.203-.526.303-.652.282a.356.356 0 0 1-.256-.186c-.06-.113-.018-.311.066-.707l.67-3.152a.8.8 0 0 0 .03-.232.356.356 0 0 0-.045-.14.787.787 0 0 0-.162-.17L1.473 6.254c-.3-.27-.45-.406-.47-.533a.356.356 0 0 1 .099-.3c.09-.092.29-.113.693-.156L5 4.926a.787.787 0 0 0 .23-.042.356.356 0 0 0 .119-.087c.038-.041.062-.096.11-.205l1.313-2.944Z"
					fill="#DBDBDB"
					stroke="#DBDBDB"
					strokeWidth={1.5}
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
			</svg>
			<p>{val}</p>
		</div>
	);
};

const Recipe = () => {
	return (
		<li className={s.art_recipe}>
			<div className={s.rec_img}>
				<RatingComp val="4.5" />
				<Image src={"/Coq-Au-Van.jpg"} width={200} height={200} alt="Recette" />
			</div>
			<h2 className={s.title_rec}>Coq au Vin</h2>
			<p className={s.tx_description}>
				Ragout de poulet au vin rouge avec des champignons et des herbes
				aromatiques. Un classique français qui combine des saveurs intenses et
				de l&apos;authenticité.
			</p>
			<p className={s.tx_foo}>Temps de préparation: 20 min</p>
			<div className={s.rec_btns}>
				<button className={s.btn_save}>
					<Image src={iconSave} alt="Save" />
				</button>
				<Link className={s.link_info_reci} href="/rece/4321">
					Plus d&apos;informations
					<svg
						width={24}
						height={24}
						viewBox="0 0 24 24"
						fill="none"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M9.97 7.47a.75.75 0 0 1 1.06 0l4 4a.75.75 0 0 1 0 1.06l-4 4a.75.75 0 1 1-1.06-1.06L13.44 12 9.97 8.53a.75.75 0 0 1 0-1.06Z"
							fill="#fff"
						/>
					</svg>
				</Link>
			</div>
		</li>
	);
};

export default function ReceMenu() {
	return (
		<section className={s.recipes_container}>
			<article className={s.art_categ}>
				<div>
					<h1>Menu de recettes</h1>
					{/* <span className={s.btn_search}>
            <svg
              width={24}
              height={24}
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M14.385 15.446a6.75 6.75 0 1 1 1.06-1.06l5.156 5.155a.75.75 0 1 1-1.06 1.06l-5.156-5.155Zm-7.926-1.562a5.25 5.25 0 1 1 7.43-.005l-.005.005-.005.004a5.25 5.25 0 0 1-7.42-.004Z"
                fill="#000"
              />
            </svg>
          </span> */}
				</div>
				<ul className={s.cont_categ}>
					<li>Entrées</li>
					<li>Plats Principaux</li>
					<li>Desserts</li>
					<li>Apéritifs</li>
					<li>Viandes</li>
					<li>Poissons et Fruits de Mer</li>
					<li>Soupes et Crèmes</li>
					<li>Salades</li>
					<li>Pâtes</li>
					<li>Végétariennes</li>
					<li>Véganes</li>
					<li>Petit-déjeuners</li>
					<li>Boissons</li>
					<li>Accompagnements</li>
				</ul>
				<div className={s.ry_div_cat} />
			</article>
			
			<h3 className={s.subtitle_reci}>Recettes populaire</h3>
			<ul className={s.cont_recipes}>
				<Recipe />
				<Recipe />
				<Recipe />
				<Recipe />
				<Recipe />
				<Recipe />
				<Recipe />
				<Recipe />
			</ul>
		</section>
	);
}
