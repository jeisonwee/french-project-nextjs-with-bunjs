import LayoutPrin from "@/components/LayoutPrin";
import "./globals.css";

export const metadata = {
  title: "Site Web Simple Francais",
  description:
    "Découvrez des délicieuses recettes françaises à préparer chez vous. Trouvez des recettes classiques françaises comme le Coq au Vin, la Ratatouille et bien plus encore.",
};

export default function RootLayout({ children }) {
  return (
    <html lang="fr">
      <body>
        <style>
          {`
          :root {
            --cl-main: #FF4000;
            --background-main: #ffffff;
            --contrast-main: #000000;
            --contrast-mid: #808080;
            --contrast-mid-t: rgba(0, 0, 0, 0.50);
            --cl-ry: #EBEBEB;
            --contrast-800: #333333;
            --contrast-600: #999999;
            --contrast-600-t: rgba(255, 255, 255, 0.6);
          }
        `}
        </style>
        <LayoutPrin>{children}</LayoutPrin>
      </body>
    </html>
  );
}
