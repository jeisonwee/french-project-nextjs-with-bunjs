import Image from "next/image";

// CSS Ressources
import s from "../css/main.module.css";

// Ressources
import bannerHome from "../../public/photo_main.jpeg";
import lgIsotype from "../../public/icons/icon_SF_home.svg";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <section className={s.section_banner}>
        <div className={s.banner_cont}>
          <div className={s.banner_float_cont}>
            <Image src={lgIsotype} alt="Logo SF" width={32} />
            <h1>Bienvenue dans l&apos;Art de la Cuisine Française</h1>
            <p>
              Sous le titre principal, ajoutez un message de bienvenue plus
              détaillé. Parlez de la passion pour la cuisine française et de ce
              qui rend notre site web unique.
            </p>
            <Link className={s.btn_discover} href="/rece">
              Découvrez des recettes
            </Link>
          </div>
          <Image className={s.banner_backg} src={bannerHome} alt="Banner Home" />
        </div>
      </section>
    </>
  );
}
