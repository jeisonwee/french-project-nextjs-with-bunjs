"use client";

import { useState } from "react";
import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";

// CSS ressources
import s from "@/css/header.module.css";

// Ressources
import { txFooGen } from "./FooterMain";

const Header = () => {
  const pathname = usePathname();

  // Statut des options
  const [stateNavOpt, setStateNavOpt] = useState(false);

  // Icône de guidage droit
  const IconRight = () => {
    return (
      <svg
        width={24}
        height={24}
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M9.97 7.47a.75.75 0 0 1 1.06 0l4 4a.75.75 0 0 1 0 1.06l-4 4a.75.75 0 1 1-1.06-1.06L13.44 12 9.97 8.53a.75.75 0 0 1 0-1.06Z"
          fill=""
        />
      </svg>
    );
  };

  // Liste des options
  const optList = [
    { title: "Home", path: "/" },
    { title: "Recettes", path: "/rece" },
    { title: "À Propos de Nous", path: "/about" },
    { title: "Contact", path: "/contact" },
    { title: "Blog", path: "/blog" },
  ];

  // Menu des options
  const MenuItem = ({ title, path, key }) => {
    const isActive = pathname === path;

    return (
      <>
        <li className={isActive ? `${s.navbar_opc_selected}` : ""}>
          <Link href={path}>
            {title}
            <IconRight />
          </Link>
        </li>
        <span />
      </>
    );
  };

  const NavOptions = () => {
    return (
      <nav className={s.navbar_cont}>
        <ul>
          {optList.map((opcion, index) => (
            <MenuItem key={index} title={opcion.title} path={opcion.path} />
          ))}
        </ul>
      </nav>
    );
  };

  // Responsive -------
  const MenuItemRes = ({ title, path }) => {
    const isActive = pathname === path;

    return (
      <li>
        <Link
          href={path}
          className={isActive ? `${s.navbar_opc_selected_res}` : ""}
        >
          {title}
          <IconRight />
        </Link>
      </li>
    );
  };

  return (
    <>
      <header className={s.header_main}>
        <Link href="/">
          <Image
            className={s.hder_logo}
            src={"/Saveurs-logo.svg"}
            width={128}
            height={18}
            alt="Logo Saveurs"
          />
        </Link>

        <div
          className={s.cont_opt_nav}
          onClick={() => setStateNavOpt(!stateNavOpt)}
        >
          <button className={s.btn_opcs}>
            {stateNavOpt ? (
              <svg
                width={24}
                height={24}
                style={{ background: "var(--cl-ry)" }}
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="m8.464 15.535 7.072-7.07m-7.072 0 7.072 7.07"
                  stroke="var(--contrast-800)"
                  strokeWidth={1.5}
                  strokeLinecap="round"
                />
              </svg>
            ) : (
              <svg
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6 10.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3Zm4.5 1.5a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0Zm6 0a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0Z"
                  fill="var(--contrast-800)"
                />
              </svg>
            )}
          </button>
          {stateNavOpt ? <NavOptions /> : null}
        </div>
      </header>

      <header className={s.header_main_res}>
        <Link href="/" className={s.link_h1}>
          <Image
            src={"/Saveurs-logo.svg"}
            width={129}
            height={18}
            alt="Logo Saveurs"
          />
        </Link>
        <nav>
          <ul className={s.list_opts}>
            {optList.map((opcion, index) => (
              <MenuItemRes
                key={index}
                title={opcion.title}
                path={opcion.path}
              />
            ))}
          </ul>
        </nav>
        <footer className={s.hder_foo}>
          <p>{txFooGen}</p>
        </footer>
      </header>
    </>
  );
};

export default Header;
