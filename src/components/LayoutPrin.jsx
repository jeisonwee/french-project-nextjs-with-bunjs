import Header from "@/components/Header";
import FooterMain from "./FooterMain";

// CSS ressources
import s from "@/css/layout.module.css";

const LayoutPrin = ({ children }) => {
  return (
    <main className={s.main_layout}>
      <Header />
      <section className={s.page_content}>{children}</section>
      <FooterMain />
    </main>
  );
};

export default LayoutPrin;
