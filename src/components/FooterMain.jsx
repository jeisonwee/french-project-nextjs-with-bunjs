import Image from "next/image";

// CSS Ressources
import s from "@/css/footermain.module.css";

// Ressources
import lgIsotype from "../../public/icons/icon_SF.svg";

// Données exportées à des fins générales
export let txFooGen = "Copyright © 2024 Jeisson León | Lion Team Org - Tous droits réservés - Moralba, Co..";

const FooterMain = () => {
  return (
    <footer className={s.foo_main}>
      <div>
        <Image src={lgIsotype} alt="Logo Isotype" />
        <h3
          className={s.foo_title}
          style={{ fontFamily: "'Pacifico', cursive" }}
        >
          Saveurs Françaises
        </h3>
      </div>
      <div className={s.ry_div} />
      <p>{txFooGen}</p>
    </footer>
  );
};

export default FooterMain;
